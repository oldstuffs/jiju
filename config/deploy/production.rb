set :stage, :production

set :deploy_to, "/home/jiju/jiju"
set :branch, "master"

set :puma_role, :app
set :puma_config_file, "config/puma.rb"

server 'jiju.co', user: 'jiju', roles: %w{web app db}, my_property: :my_value

