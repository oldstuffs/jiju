# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
# Keep the secret key in config/secret.key
secret_file = File.expand_path('config/secret.key', Rails.root)
 
unless File.exists?(secret_file)
  puts 'Missing config/secret.key file. Please run:'
  puts '  $ rake secret > config/secret.key'
  exit 1
end
 
Jiju::Application.config.secret_key_base = File.read(secret_file).chomp
