APP_ROOT = '/home/jiju/jiju'
pidfile "#{APP_ROOT}/shared/tmp/pids/puma.pid"
state_path "#{APP_ROOT}/shared/tmp/pids/puma.state"
bind "unix:///tmp/jiju.sock"
daemonize true
workers 2
threads 8,32
preload_app!